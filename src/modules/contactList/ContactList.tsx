import React, { useState, useEffect } from "react";
import { useQuery, useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Button from "@material-ui/core/Button";
import { ContactEditor } from "../contactEditor/ContactEditor";
import './contactList.css'
import { setDate } from "../utilities";

export type Contact = {
  id: string;
  name: string;
  email: string;
  modified: string;
  created: string;
};

const GET_CONTACTS = gql`
  query {
    contacts{
      name
      email
      id
    }
  }
`

const REMOVE_CONTACT = gql`
  mutation removeContact($id: ID){
    deleteContact(id: $id)
  }
`

const ADD_CONTACT = gql`
  mutation addContact($id: ID, $name: String, $email: String, $modified: String, $created: String){
    addContact(contact: {id: $id, name: $name, email: $email, modified: $modified, created: $created}){
      id
      name
      email
      modified
      created
    }
  }
`

export const ContactList = () => {
  const [isEditorOpen, setEditor] = useState(false);
  const [selectedId, setId] = useState('');
  const {data, loading, error, refetch } = useQuery(GET_CONTACTS)
  const [removeContact] = useMutation(REMOVE_CONTACT)
  const [addContact] = useMutation(ADD_CONTACT)
  const contactList: Contact[] = data && data.contacts

  useEffect(() => { refetch() })
  
  if (loading) return <p>LOADING</p>;
  if (error) return <p>ERROR</p>;


  const onCreateContact = () => {
    const id = `contact_${Date.now()}`;
    const date = setDate();
    addContact({ variables: {
      id,
      name: "not provided",
      email: "not provided",
      modified: date,
      created: date
    }})
    refetch()
  };

  const onRemoveContact = (id: string) => {
    removeContact({ variables: { id } })
    refetch()
  };

  return (
    <div className="contact-list">
      {contactList.map(contact => (
        <div key={contact.id} className="contact-list-row">
          <p>{contact.name}</p>
          <p>{contact.email}</p>
          <div className={"contact-list-buttons"}>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => {
                onRemoveContact(contact.id);
              }}
            >
              Delete
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setEditor(true);
                setId(contact.id);
              }}
            >
              Edit
            </Button>
          </div>
        </div>
      ))}

      <div className="under-list">
        <Button variant="contained" onClick={onCreateContact}>
          Add contact
        </Button>
      </div>
      
      {isEditorOpen && <ContactEditor
        isOpen={isEditorOpen}
        setEditor={setEditor}
        contactId={selectedId}
      />}
    </div>
  );
};
