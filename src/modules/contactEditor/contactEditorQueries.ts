import gql from "graphql-tag";

export const GET_CONTACT = gql`
  query getContact($id: ID){
    contact(id: $id){
      name
      email
      id
      modified
      created
    }
  }
`

export const UPDATE_CONTACT = gql`
  mutation updateContact($id: ID, $name: String, $email: String, $modified: String, $created: String){
    updateContact(contact: {id: $id, name: $name, email: $email, modified: $modified, created: $created}){
      id
      name
      email
      modified
      created
    }
  }
`