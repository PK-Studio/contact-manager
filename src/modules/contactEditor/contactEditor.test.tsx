import React from "react";
import { render, fireEvent, cleanup } from "@testing-library/react";
import { MockedProvider } from '@apollo/react-testing'
import { ContactEditor } from "./ContactEditor";
import {GET_CONTACT, UPDATE_CONTACT} from './contactEditorQueries'

describe("Contact editor", () => {
  const renderComponent = (partialProps = {}) => {
    const props = {
      isOpen: true,
      setEditor: () => {},
      contactId: '',
      ...partialProps
    };
    const mocks = [
      {
        request: {
          query: GET_CONTACT,
          variables: {
            id: 'test_id',
          },
        },
        result: { data: {
          contact: {id: 'test_id', name: 'test name', email: 'test@email.com', modified: 'test date', created: 'test date'}
        }}
      },
      {
        request: {
          query: UPDATE_CONTACT,
          variables: {
            id: 'test_id',
            name: 'test name',
            email: 'test@email.com',
            modified: 'test date',
            created: 'test date'
          },
        },
        result: { data: {
          contact: {id: 'test_id', name: 'test name', email: 'test@email.com', modified: 'test date', created: 'test date'}
        }}
      }
    ]

    return render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <ContactEditor {...props} />
    </MockedProvider>
    );
  };

  afterEach(cleanup);

  it("sets editor to false on close button click", () => {
    const setEditor = jest.fn();
    const { getByText } = renderComponent({ setEditor });
    fireEvent.click(getByText("Close"));
    expect(setEditor).toBeCalledTimes(1);
    expect(setEditor).toBeCalledWith(false);
  });

  it("call updateName when name changes", () => {
    const updateName = jest.fn();
    const value = "Test new name";
    const { getByLabelText } = renderComponent({ updateName });
    fireEvent.change(getByLabelText("Name"), { target: { value } });
    expect(updateName).toBeCalledWith(value);
  });

  it("call updateEmail when email changes", () => {
    const updateEmail = jest.fn();
    const value = "test.new.email.address@gmail.com";
    const { getByLabelText } = renderComponent({ updateEmail });
    fireEvent.change(getByLabelText("Email"), { target: { value } });
    expect(updateEmail).toBeCalledWith(value);
  });
});
