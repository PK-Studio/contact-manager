import React from "react";
import Modal from "@material-ui/core/Modal";
import { Button, TextField } from "@material-ui/core";
import { useQuery, useMutation } from "react-apollo";
import { Contact } from "../contactList/ContactList";
import { setDate } from "../utilities";
import {GET_CONTACT, UPDATE_CONTACT} from './contactEditorQueries'
import './contactEditor.css'

type ContactEditorProps = {
  isOpen: boolean;
  contactId?: String
  setEditor: (state: boolean) => void;
}

export const ContactEditor = ({
  isOpen,
  contactId,
  setEditor
}: ContactEditorProps) => {
  const {data, loading, error } = useQuery(GET_CONTACT, {
    variables: {id: contactId}
  })
  const [updateContact] = useMutation(UPDATE_CONTACT)
  

  if (loading) return <p>LOADING</p>;
  if (error) return <p>ERROR</p>;

  const {contact} = data
  const {name, email, modified, created } = contact as Contact

  const onSubmit = (event: any) => {
    event.preventDefault()
    
    const modified = setDate();
    const name = event.target.querySelector('#contact-editor-name').value
    const email = event.target.querySelector('#contact-editor-email').value

    updateContact({variables: {
      ...contact,
      modified,
      name,
      email
    }});

    setEditor(false)
  }

  return (
    <div>
      <Modal
        open={isOpen}
        onClose={() => {
          setEditor(false);
        }}
      >
        <div className="contact-editor">
          <h3>Contact details</h3>

          <form id="contact_form" className="contact-form" autoComplete="off" onSubmit={onSubmit}>
            <TextField            
              id="contact-editor-name"
              label="Name"
              aria-labelledby="contact-editor-name"
              margin="normal"
              defaultValue={name}
              required
            />

            <TextField
              id={"contact-editor-email"}
              label="Email"
              aria-labelledby="contact-editor-email"
              margin="normal"
              type="email"
              defaultValue={email}
              required
            />

            <TextField
              disabled
              id={"contact-editor-email"}
              label="Created"
              aria-labelledby="contact-editor-email"
              value={created}
              margin="normal"
            />

            <TextField
              disabled
              id={"contact-editor-email"}
              label="Last modified"
              aria-labelledby="contact-editor-email"
              value={modified}
              margin="normal"
            />
          </form>

          <Button
            variant="contained"
            type="submit"
            form="contact_form"
          >
            Close
          </Button>
        </div>
      </Modal>
    </div>
  );
};
